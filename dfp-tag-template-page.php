<?php
/*
Template name: DFP Tag Page template 
*/
 get_header();
?>
    <!-- #content Starts -->
    <?php
                global $wp_query;

                if(isset($wp_query->query_vars['dfp_tag']) && !empty($wp_query->query_vars['dfp_tag'])) {
                        $the_dfp_tag = strtolower($wp_query->query_vars['dfp_tag']);
                        $matched_dfp_tag_value = '';

                        $dfp_tags = get_option('dfp_tags');
                        foreach($dfp_tags as $d) {
                                $dfp_tag = strtolower(str_replace(' ', '-', str_replace('&', '-', $d)));
                                if($dfp_tag == $the_dfp_tag) {
                                        $matched_dfp_tag_value = $d;
                                        break;
                                }
                        }
						
                        if(!empty($matched_dfp_tag_value)) {
							$paged = explode('/', $_SERVER['REQUEST_URI']);
							if(!empty($paged[4])) {
								$paged = $paged[4];
							}else {
								$paged = 1; 
							}
                			//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			
			if($matched_dfp_tag_value == 'hospitalitykiakaha') {
				header('location: '.site_url().'/hospitalitykiakaha/'); 
				exit; 
			}
                //echo $paged;
                $args = array ( 'meta_key' => 'post_dfp_tag', 'posts_per_page' => '14','paged' => $paged, 'meta_value'=> $matched_dfp_tag_value , 'orderby'=>'date', 'order'=>'desc', 'meta_compare'=>'=');
                          query_posts( $args );
                                /*
                                echo '<pre>';
                                print_r($GLOBALS['wp_query']);
                                echo '</pre>';
                                */
                        }else {
                                //redirect to home page 
                                header('location: ' . site_url());
                                exit;
                        }
                }
                else {
                        //redirect to home page
                        header('location: ' . site_url());
                        exit;
                }
        ?>

<div id="content" class="archive_page mt-5 clearfix">
        <div id="main" class="clearfix" role="main">
            <div class="container">
				<div class="page-header d-flex justify-content-between">
					<h1><?php echo $matched_dfp_tag_value;?></h1>
				</div>
				
			                <div class="row">
                    <div class="col-lg-8">
                        <div class="scroller">
                            <?php if (have_posts()) :
                            while (have_posts()) : the_post(); ?>

                                <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix post'); ?> role="article">
                                    <div class="mediafy row">
                                        <div class="col-md-4"><?php the_post_thumbnail('med_size', array('class' => 'align-self-center mr-3')); ?></div>
                                        <div class="media-bodyfy col-md-8">
                                            <h2 class="mt-0"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                                                <?php the_excerpt(); ?>
                                            <em class="meta_data">Posted on: <?php echo get_post_time('j F, Y', true) ?></em>
                                        </div>
                                    </div>
                                    <?php $the_post_type = get_post_type(get_the_ID()); if($the_post_type == 'sponsoredcontent'){?>
                                    <script type="text/javascript">
                                        <?php
                                        if(function_exists('intermedia_ga_event')) {
                                                echo intermedia_ga_event('Impression', 'Sponsor Content Impression', get_the_title(get_the_ID()));
                                        }
                                        ?>
                                    </script>
                                    <?php } ?>
                                </article> <!-- end article -->
                            <?php endwhile; ?>
                        </div>
                        <!-- status is at bottom of scroll -->
                        <div class="page-load-status">
                            <div class="loader-ellips infinite-scroll-request">
                                LOADING....
                            </div>
                            <p class="infinite-scroll-last">End of content</p>
                            <p class="infinite-scroll-error">No more pages to load</p>
                        </div>


                        <?php if (show_posts_nav()) : ?>
                            <p class="align-self-center ">
                                <button class="view-more-button btn btn-secondary">View more</button>
                            </p>

                            <nav class="pagination">
                                <ul class="pager">
                                    <li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "intermedia")) ?></li>
                                    <li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "intermedia")) ?></li>
                                </ul>
                            </nav>
                        <?php endif; ?>
                        <?php else : ?>
                            <article id="post-not-found">
                                <header>
                                    <h1><?php _e("No Posts Yet", "intermedia"); ?></h1>
                                </header>
                                <section class="post_content">
                                    <p><?php _e("Sorry, What you were looking for is not here.", "intermedia"); ?></p>
                                </section>
                                <footer></footer>
                            </article>
                        <?php endif; ?>
                    </div>

                    <div class="col-lg-4">
                        <?php get_sidebar(); // sidebar 1 ?>
                    </div>
                </div>
            </div>
        </div> <!-- end #main -->

<?php get_footer(); ?>
