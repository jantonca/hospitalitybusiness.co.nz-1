<?php
/**
 * Theme functions
 */
add_action( 'wp_enqueue_scripts', 'newspack_child_enqueue_styles' );
function newspack_child_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    // include the css file
    $cssFilePath = glob( get_stylesheet_directory() . '/css/build/main.min.*' );
    $cssFileURI = get_stylesheet_directory_uri() . '/css/build/' . basename($cssFilePath[0]);
    wp_enqueue_style( 'child-style-min', $cssFileURI );
    // include the javascript file
    $jsFilePath = glob( get_stylesheet_directory() . '/js/build/app.min.*.js' );
    $jsFileURI = get_stylesheet_directory_uri() . '/js/build/' . basename($jsFilePath[0]);
    wp_enqueue_script( 'child-scripts-min', $jsFileURI , null , null , true );
}
// remove default guttenberg block editor stylesheet
//  https://wpassist.me/how-to-remove-block-library-css-from-wordpress/
function wpassist_remove_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
} 
add_action( 'wp_enqueue_scripts', 'wpassist_remove_block_library_css' );
/**********************************************************************************************/
/**
 * Custom widget for the header
 *
 */
function intermedia_header_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Header', 'newspack' ),
			'id'            => 'header',
			'description'   => __( 'Add widgets here to appear in your header.', 'newspack' ),
			'before_widget' => '<section id="leaderboard-widget">',
			'after_widget'  => '</section>'
		)
	);

}
add_action( 'widgets_init', 'intermedia_header_widgets_init' );

//Update jquery version
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );
function replace_core_jquery_version() {
    wp_deregister_script( 'jquery-core' );
    wp_register_script( 'jquery-core', "https://code.jquery.com/jquery-3.1.1.min.js", array(), '3.1.1' );
    wp_deregister_script( 'jquery-migrate' );
    wp_register_script( 'jquery-migrate', "https://code.jquery.com/jquery-migrate-3.0.0.min.js", array(), '3.0.0' );
}