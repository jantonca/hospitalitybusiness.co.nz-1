<?php 
/*
Template name: hospitalitykiakaha.php
*/

get_header();

$home_page_thumbnail_id = get_option( 'page_on_front' );

$list_of_terms = array(
        array(
                'name'  =>      'Support & Advice',
                'slug'  =>      'support-advice',
                'id'    =>      '9005'
        ),

        array(
                'name'  =>      'Partner Content',
                'slug'  =>      'partner-content',
                'id'    =>      '9007'
        ),

        array(
                'name'  =>      'Government News',
                'slug'  =>      'government-news',
                'id'    =>      '9009'
        ),

        array(
                'name'  =>      'Products & Offers',
                'slug'  =>      'products-offers',
                'id'    =>      '9008'
        ),

        array(
                'name'  =>      'Ideas & Initiatives',
                'slug'  =>      'ideas-initiatives',
                'id'    =>      '9006'
        ),

	array(
                'name'  =>      'Profiles',
                'slug'  =>      'profiles',
                'id'    =>      '8702'
	)
);

$terms_total = count($list_of_terms);
$all_terms = $list_of_terms;
?>

        <section id="primary" class="content-area <?php echo esc_attr( newspack_get_category_tag_classes( get_the_ID() ) ); ?>">
                <main id="main" class="site-main">
			<div class="main-content">

<section class="marron-tag wrapper tiles-row">
        <div class="container" style="text-align:center;position: relative;top: 30px;margin-bottom: 30px;clear:both;">
                <div class="row">
                        <h3 style="font-size:30px;margin-bottom:20px;">#HospitalityKiaKaha Resource Hub</h3>
                </div>
                <p style="text-align:justify;">Hospitality Business is pleased to announce the launch of our new information hub – <strong>#HospitalityKiaKaha</strong> – designed to support our industry as we navigate through COVID-19.</p>
                <p style="text-align:justify;">Reflecting the strength and resilience of hospitality in New Zealand the hub is a resource dedicated to bringing the latest news and information vital to the industry as we navigate through unprecedented times.</p>
                <p style="text-align:justify;"><strong>#HospitalityKiaKaha</strong> is split into five sections: Support & Advice, Partner Content, Government News, Products & Offers, Ideas & Initiatives and Profiles. </p>
                <p style="text-align:justify;">If you’re looking for information on Government support, industry association activities, need tips for implementing new food delivery options or just want to read about how operators are adapting their operations – the hub is the go-to for daily dispatches.</p>
                <p style="text-align:justify;">Hospitality Business magazine, thanks to the support of our sponsors, will continue to share the stories of the people who make up our industry and provide the latest news and developments.</p>
                <p style="text-align:justify;">If you’re offering takeaway or delivery, have introduced new menu items or just want to get the word out that you’re open for business, tag #hospitalitybusiness and #HospitalityKiaKaha on social media.</p>
                <p style="text-align:justify;">To get the news delivered straight to your inbox, sign up for our newsletter <a href="https://www.hospitalitybusiness.co.nz/subscribe/" target="_blank">here</a> and check the hub for daily updates.</p>
                <p style="text-align:justify;">To pitch stories contact Editor, Kimberley Dixon at <a href="mailto:kdixon@intermedianz.co.nz">kdixon@intermedianz.co.nz</a></p>
        </div>
</section>

<?php
for($x = 0; $x < count($all_terms); $x++) {
 ?>

<section class="marron-tag wrapper tiles-row">
<div class="container" style="clear:both;">
        <div class="row">
                <a href="<?php echo get_category_link($all_terms[$x]['id']); ?>" target="_blank" class="heading-tag">
                        <h3><?php echo $all_terms[$x]['name']; ?></h3>
                        <span>
                                <div class="table">
                                        <div class="table-cell">
                                                View All
                                        </div>
                                </div>
                        </span>
                </a>
        </div>

<?php
$args = array(
        'post_type'             =>      array('sponsoredcontent', 'post'),
        'cat'                   => $all_terms[$x]['id'],
        'meta_key'              => 'post_dfp_tag',
        'meta_value'            => 'HospitalityKiaKaha',
        'orderby'               => 'date',
        'order'                 => 'desc',
        'meta_compare'          => '='
);

        $each_video_post = new WP_Query($args);

        $total_each_video_post = $each_video_post->found_posts;
?>

<?php if ($each_video_post->have_posts()) { ?>
<div class="row">
<?php
while($each_video_post->have_posts()) { $each_video_post->the_post();
        $current_post_num = $each_video_post->current_post + 1;
        if (has_post_thumbnail()){
                $feature_image = wp_get_attachment_image_src(get_post_thumbnail_id($each_video_post->ID), 'medium')[0];
        } else {
                $feature_image = wp_get_attachment_image_src(get_post_thumbnail_id($home_page_thumbnail_id), 'medium')[0];
        }
if ($current_post_num == 1){
?>
        <div class="col-2">
                <a href="<?php echo the_permalink(); ?>">
                        <div class="video" style="background-image: url('<?php echo $feature_image; ?>')">
                                <div class="table">
                                        <div class="table-cell">
                                        </div>
                                </div>
                        </div>
                        <div class="video-heading">
                                <?php echo get_the_title(); ?>
                        </div>
                </a>
        </div>
<?php } ?> <!-- end of number 1 -->

<?php } ?>

<?php if ($each_video_post->have_posts()) { ?>
<div class="col-2 related-videos">
<?php


while($each_video_post->have_posts()) { $each_video_post->the_post();
        $current_post_num = $each_video_post->current_post + 1;
        if (has_post_thumbnail()){
                $feature_image = wp_get_attachment_image_src(get_post_thumbnail_id($each_video_post->ID), 'thumbnail')[0];
        } else {
                $feature_image = wp_get_attachment_image_src(get_post_thumbnail_id($home_page_thumbnail_id), 'thumbnail')[0];
        }
if (($current_post_num > 1) && ($current_post_num < 5)){
?>
        <a href="<?php echo the_permalink(); ?>" class="each-video">
                <div class="related-video-image" style="background-image: url('<?php echo $feature_image; ?>');">
                        <div class="table">
                                <div class="table-cell">
                                </div>
                        </div>
                </div>
                <div class="related-video-text">
                        <?php echo get_the_title(); ?>
                </div>
        </a>
<?php } ?> <!-- end of number 1 -->

<?php } ?>
</div>

<?php } ?>

</div> <!-- end of row -->
<?php } ?>
</div> <!-- container -->
</section>

<?php if($x == 0) { ?>
<section class="marron-tag wrapper tiles-row">
<div class="container" style="position: relative;top: 30px;clear:both;background: #fff;padding-top: 30px;margin-bottom: 40px;padding-bottom: 10px;">
        <h4 style="text-align:center;">Sign up to our newsletter for COVID-19 updates and advice</h4>
        <div>
        <?php echo do_shortcode('[gravityform id=5 description=false title=false ajax=true]');?>
        </div>
</div>
</section>
<?php } ?>

<?php } ?> <!-- end of for loop each category -->
<style type="text/css">
.marron-tag .related-videos {height: 233px !important;max-width: 100% !important;}
.marron-tag .container {
    padding-left: 30px;
    padding-right: 30px;
    width: 100%;
    margin: auto;
    min-width: 290px;
}
.marron-tag .row {
    width: 100%;
    float: left;
}
.marron-tag .heading-tag {
    background-color: #c4161c;
    color: white;
}
.marron-tag.tiles-row a {
    text-decoration: none;
}
.marron-tag .heading-tag {
    float: left;
    width: 100%;
    text-transform: uppercase;
    -webkit-transition: all .3s;
    -moz-transition: all .3s;
    -ms-transition: all .3s;
    -o-transition: all .3s;
    transition: all .3s;
    -webkit-transition-timing-function: ease-in;
    -moz-transition-timing-function: ease-in;
    -ms-transition-timing-function: ease-in;
    -o-transition-timing-function: ease-in;
    transition-timing-function: ease-in;
    transform: translateY(0);
    position: relative;
    font-weight: bold;
}
.marron-tag .heading-tag span {
    float: right;
    font-size: 12px;
    position: absolute;
    top: 0;
    height: 100%;
    right: 15px;
}
.marron-tag .table {
    display: table;
    width: 100%;
    height: 100%;
}
.marron-tag .heading-tag span .table-cell {
    padding: 4px 0;
}
.marron-tag .table-cell {
    display: table-cell;
    vertical-align: middle;
    float: none;
}

.marron-tag.wrapper {
    padding-top: 20px;
    padding-bottom: 20px;
    background-color: #f6f6f6;
}

.marron-tag.tiles-row .col-2:nth-last-of-type(1), .tiles-row .col-2:nth-last-of-type(2) {
    margin-bottom: 0;
}
.marron-tag.tiles-row .col-2:first-child {
    clear: left;
}
.marron-tag.tiles-row .col-2 {
    float: left;
    width: 48.9%;
    margin-right: 2.2%;
    margin-bottom: 0;
}
.marron-tag .col-2:nth-last-of-type(1), .col-2:nth-last-of-type(2) {
    margin-bottom: 0;
}
.marron-tag .col-2:first-child {
    clear: left;
}
.marron-tag .col-2 {
    float: left;
    width: 50%;
    margin-right: 0;
    margin-bottom: 0;
}
.marron-tag .video {
    position: relative;
    height: 200px;
}
.marron-tag .video-heading {
    width: 100%;
    color: #232323;
    font-weight: 600;
    padding: 17px 10px 0 0;
    text-transform: capitalize;
    letter-spacing: .5px;
    border-left: 1px solid;
    border-right: 1px solid;
    border-bottom: 1px solid;
    border-color: #eee;
    line-height: 1.3;
    padding: 3px 8px;
    overflow: hidden;
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -ms-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
    background-color: #fefefe;
}
.marron-tag.tiles-row .col-2:nth-last-of-type(1), .marron-tag.tiles-row .col-2:nth-last-of-type(2) {
    margin-bottom: 0;
}
.marron-tag.tiles-row .col-2:nth-of-type(2n+2) {
    margin-right: 0;
}
.marron-tag.tiles-row .col-2 {
    float: left;
    width: 48.9%;
    margin-right: 2.2%;
    margin-bottom: 0;
}
.marron-tag .col-2:nth-last-of-type(1), .col-2:nth-last-of-type(2) {
    margin-bottom: 0;
}
.marron-tag .col-2:nth-of-type(2n+2) {
    margin-right: 0;
}
.marron-tag .related-videos {
    height: 233px !important;
    max-width: 100% !important;
}
.marron-tag .related-videos {
    max-width: 300px;
    height: 261px;
}
.marron-tag .col-2 {
    float: left;
    width: 50%;
    margin-right: 0;
    margin-bottom: 0;
}
.marron-tag .related-videos .each-video:first-child {
    margin-top: 0;
}
.marron-tag .related-videos .each-video {
    float: left;
    width: 100%;
    height: 28.13%;
    margin-top: 6.7%;
    -webkit-transition: all .3s;
    -moz-transition: all .3s;
    -ms-transition: all .3s;
    -o-transition: all .3s;
    transition: all .3s;
    -webkit-transition-timing-function: ease-out;
    -moz-transition-timing-function: ease-out;
    -ms-transition-timing-function: ease-out;
    -o-transition-timing-function: ease-out;
    transition-timing-function: ease-out;
    transform: translateY(0);
}
.marron-tag .related-video-image {
    width: 80px;
    float: left;
    height: 100%;
}
.marron-tag .related-video-text {
    max-width: 80% !important;
}
.marron-tag .related-video-text {
    float: left;
    width: 220px;
    height: 100%;
    font-weight: 600;
    font-size: 15px;
    color: #232323;
    border-top: 1px solid #E3E3E3;
    border-right: 1px solid #E3E3E3;
    border-bottom: 1px solid #E3E3E3;
    background-color: #FFF;
    -webkit-transition: all .3s;
    -moz-transition: all .3s;
    -ms-transition: all .3s;
    -o-transition: all .3s;
    transition: all .3s;
    -webkit-transition-timing-function: ease-out;
    -moz-transition-timing-function: ease-out;
    -ms-transition-timing-function: ease-out;
    -o-transition-timing-function: ease-out;
    transition-timing-function: ease-out;
    transform: translateY(0);
    overflow: hidden;
    line-height: 1.8;
    padding: 7.8px 15px;
}
.marron-tag .heading-tag h3 {
    color: white;
    padding: 5.5px 85px 5.5px 15px;
    font-size: 20px;
    margin: 0;
    float: left;
    width: 100%;
    position: relative;
    top: -1px;
    text-align: left;
}

.marron-tag .row:not(:first-of-type) {
    margin-top: 20px;
}

.marron-tag form#gform_5, #gform_confirmation_message_5 {
	text-align:center !important;
}
/* RESPONSIVE JOSE STYLES */


@media only screen and (max-width: 900px) {

	.marron-tag.tiles-row .col-2 {

		float: none;

		width: 100%;

		margin-right: 0;

		margin-bottom: 1rem!important;

	}

	.marron-tag .video {

		height: 320px;

	}

	.marron-tag .related-video-text {

		float: none;

		width: 100% !important;

	}

	.marron-tag .related-videos .each-video {

		float: left;

		width: 100%;

		height: 30%;

		margin-top: 1rem;

	}

	.marron-tag .related-video-image {

		width: 25%;

		float: left;

		height: 100%;

	}

	.marron-tag .related-video-text {

		float: none;

		width: 75% !important;

	}

}

@media only screen and (max-width: 600px) {

	.marron-tag .video {

		height: 160px;

	}


}
</style>

</div><!-- .main-content -->
                       <?php
                                get_sidebar();
                        ?>

                </main><!-- #main -->
        </section><!-- #primary -->

<?php
get_footer();


